'use strict';

var server = require('server');


server.get('Show', function (req, res, next) { //creating a simple endpoint which you can find by https://localhost/on/demandware.store/Sites-RefArch-Site/default/Hello-Show and registers the Show route for the Home module
    
    var renderingData = {
        name: 'testname',
        
    }
    //res.json({cart: renderingData});
    res.render('helloTemplate', {renderingData: renderingData}); // returning in the response template 'hello/helloTemplate' with renderingData
    next();//notifies middleware chain that it can move to the next step or terminate if this is the last step.
});




server.get("Product", function (req, res, next) {
    var template = "product";
    var myproduct = { pid: "25752235M" };
    var ProductFactory = require("*/cartridge/scripts/factories/product");
    var product = ProductFactory.get(myproduct);
    res.render('helloTemplate', {product: product});
    next();
});

server.get("Loop", function (req, res, next) {
    var loop = ["1","2","3","4"];
    res.render('helloTemplate', {loop: loop});
    next();
});



server.get('Basket', function (req, res, next) {

    var BasketMgr = require('dw/order/BasketMgr');
        var Basket = BasketMgr.getCurrentBasket();
        var basketProducts = Basket.allProductLineItems;
        res.render('helloTemplate', {basketProducts: basketProducts});
    next();
});

server.get("Styled", function (req, res, next) {
	var template = "styled";
	res.render(template);
	next();
})

server.get('allCategories', function (req, res, next) {
    var CatalogMgr = require("dw/catalog/CatalogMgr");
    var ProductFactory = require("*/cartridge/scripts/factories/product");
    var ProductSearchModel = require("dw/catalog/ProductSearchModel");
    var apiProductSearch = new ProductSearchModel();
    var PagingModel = require("dw/web/PagingModel");
    var category = CatalogMgr.getCategory("womens");
    var pagingModel;
    var products = [];
    apiProductSearch.setCategoryID(category.ID);
    apiProductSearch.search();
    pagingModel = new PagingModel(
        apiProductSearch.getProductSearchHits(),
        apiProductSearch.count
    );
    pagingModel.setStart(1);
    pagingModel.setPageSize(100);
    var iter = pagingModel.pageElements;
    while (iter !== null && iter.hasNext()) {
    productSearchHit = iter.next();
    product = ProductFactory.get({
        pid: productSearchHit.getProduct().ID,
    });
    products.push(product);
    }
    res.render('helloTemplate', {products : products, category: category});
    next();
});



module.exports = server.exports();